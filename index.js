const express = require('express')
const app = express(),
      port = process.env.PORT || 3080
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const cors = require('cors');
let corsOptions = {
    origin: "http://localhost:4200"
};

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(cors(corsOptions));
// Parse requests of content-type - application/json
app.use(express.json());
// Parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// Default route
app.get("/", (req, res) => {
    res.json({ message: "Welcome to AdventureWorks application." });
});

require("./routes/products.routes")(app);
require("./routes/subcategories.routes")(app);
require("./routes/models.routes")(app);



app.listen(port, () => {
    console.log(`Serveur à l'écoute sur le port ${port}`)
})
