const mysql = require('mysql2');

const db = mysql.createConnection({
    host: "database",
    port: 3306,
    user: "vincent",
    password: "password",
    database: "adventureworks",
    insecureAuth: true
});

db.connect(error => {
    if (error) throw error;
    console.log("Successfully connected to the database.");
});

module.exports = db;
