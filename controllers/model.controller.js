const Model = require("../models/product_model.model");

// Retrieve all Models from the database.
exports.findAll = (req, res) => {
    Model.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving models."
            });
        else res.status(200).send(data);
    });
};
