const Subcategory = require("../models/product_subcategory.model");

// Retrieve all Subcategories from the database.
exports.findAll = (req, res) => {
    Subcategory.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving subcategories."
            });
        else res.status(200).send(data);
    });
};
