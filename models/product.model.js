const sql = require('../conf/sql.config');

const Product = function (product) {
    this.Name = product.Name;
    this.ProductNumber = product.ProductNumber;
    this.MakeFlag = product.MakeFlag;
    this.FinishedGoodsFlag = product.FinishedGoodsFlag;
    this.Color = product.Color;
    this.SafetyStockLevel = product.SafetyStockLevel
    this.ReorderPoint = product.ReorderPoint;
    this.StandardCost = product.StandardCost;
    this.ListPrice = product.ListPrice;
    this.Size = product.Size;
    this.SizeUnitMeasureCode = product.SizeUnitMeasureCode;
    this.WeightUnitMeasureCode = product.WeightUnitMeasureCode;
    this.Weight = product.Weight;
    this.DaysToManufacture = product.DaysToManufacture;
    this.ProductLine = product.ProductLine;
    this.Class = product.Class;
    this.Style = product.Style;
    this.ProductSubcategoryID = product.ProductSubcategoryID;
    this.ProductModelID = product.ProductModelID;
    this.SellStartDate = product.SellStartDate;
    this.SellEndDate = product.SellEndDate;
    this.DiscontinuedDate = product.DiscontinuedDate;
    this.rowguid = product.rowguid;
    this.ModifiedDate = product.ModifiedDate;
}

Product.getAll = result => {
    sql.query(
        "SELECT * FROM product LEFT JOIN productsubcategory ON product.ProductSubcategoryID = productsubcategory.ProductSubcategoryID LEFT JOIN productmodel ON product.ProductModelID = productmodel.ProductModelID", (err, res) => {
        if (err) {
            result(null, err);
            return;
        }

        result(null, res);
    });
};

Product.create = (newProduct, result) => {
    sql.query("INSERT INTO product SET ?", newProduct, (err, res) => {
        if (err) {
            result(err, null);
            return;
        }

        result(null, { id: res.insertId, ...newProduct });
    });
};

module.exports = Product;
