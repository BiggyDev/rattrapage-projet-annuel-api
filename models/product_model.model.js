const sql = require('../conf/sql.config');

const ProductModel = function (productModel) {
    this.Name = productModel.Name;
    this.CatalogDescription = productModel.CatalogDescription;
    this.Instructions = productModel.Instructions;
    this.rowguid = productModel.rowguid;
    this.ModifiedDate = productModel.ModifiedDate;
}

ProductModel.getAll = result => {
    sql.query(
        "SELECT * FROM productmodel", (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            result(null, res);
        });
};

module.exports = ProductModel;
