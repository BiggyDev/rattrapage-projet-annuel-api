const sql = require('../conf/sql.config');

const ProductSubcategory = function (productSubcategory) {
    this.ProductCategoryID = productSubcategory.ProductCategoryID;
    this.Name = productSubcategory.Name;
    this.rowguid = productSubcategory.rowguid;
    this.ModifiedDate = productSubcategory.ModifiedDate;
}

ProductSubcategory.getAll = result => {
    sql.query(
        "SELECT * FROM productsubcategory", (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            result(null, res);
        });
};

module.exports = ProductSubcategory;
