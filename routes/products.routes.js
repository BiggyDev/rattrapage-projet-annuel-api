module.exports = app => {
    const products = require("../controllers/product.controller");
    const router = require("express").Router();

    // Create a new Product
    router.post("/", products.create);
    // Retrieve all Products
    router.get("/", products.findAll);

    app.use('/products', router);
};
