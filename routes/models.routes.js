module.exports = app => {
    const models = require("../controllers/model.controller");
    const router = require("express").Router();

    // Retrieve all Models
    router.get("/", models.findAll);

    app.use('/models', router);
};
