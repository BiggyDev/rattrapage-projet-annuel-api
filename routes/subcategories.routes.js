module.exports = app => {
    const subcategories = require("../controllers/subcategory.controller");
    const router = require("express").Router();

    // Retrieve all Subcategories
    router.get("/", subcategories.findAll);

    app.use('/subcategories', router);
};
