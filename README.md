# AdventureWorks Products Management API

AdventureWorks Products Management is a platform where you can see all products stored in database and add more products thanks to a simple form.

### Informations
* This project works with **[Docker-Compose](https://docs.docker.com/compose/install/)**.

### Installation

#### Installation and container launch from *command line*.

+ First, please clone the project :
````bash
git clone https://gitlab.com/BiggyDev/rattrapage-projet-annuel-api.git
````

+ Move forward inside the project folder :
````bash
cd rattrapage-projet-annuel-api
````

+ Launch the container :

````bash
docker-compose up -d
````

### Documentation
* This project goes with  **[Swagger](https://swagger.io/)** for Node.js. This will allow you to access directly from your browser to the API documentation.

* To access the documentation, open your favorite browser to **[http://localhost:3080/api-docs](http://localhost:3080/api-docs)**.
